FROM gradle:7.5-jdk17-alpine AS build
WORKDIR /opt/recipehelper/recipes-service/
COPY ./ ./
RUN gradle bootJar --no-daemon --stacktrace

FROM eclipse-temurin:17-jre-alpine
WORKDIR /opt/recipehelper/recipes-service/
COPY --from=build /opt/recipehelper/recipes-service/build/libs/recipes-service.jar ./
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "./recipes-service.jar"]
