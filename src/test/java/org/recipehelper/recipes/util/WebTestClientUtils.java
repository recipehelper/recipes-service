package org.recipehelper.recipes.util;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.reactive.server.WebTestClient.ResponseSpec;
import org.springframework.web.reactive.function.BodyInserters;

public class WebTestClientUtils {

	public static ResponseSpec post(WebTestClient client, String uri, Object body) {
		return client.post()
			.uri(uri)
			.contentType(MediaType.APPLICATION_JSON)
			.body(BodyInserters.fromValue(body))
			.exchange();
	}

	public static <T> T postForObject(WebTestClient client, String uri, Object body, Class<T> clazz) {
		return post(client, uri, body)
			.expectBody(clazz)
			.returnResult()
			.getResponseBody();
	}

	public static ResponseSpec get(WebTestClient client, String uri) {
		return client.get()
			.uri(uri)
			.exchange();
	}

	public static <T> T getForObject(WebTestClient client, String uri, Class<T> body) {
		return get(client, uri)
			.expectBody(body)
			.returnResult()
			.getResponseBody();
	}

	public static <T> List<T> getForCollection(WebTestClient client, String uri, Class<T> clazz) {
		return get(client, uri)
			.expectBodyList(clazz)
			.returnResult()
			.getResponseBody();
	}

	public static ResponseSpec expectError(ResponseSpec spec, int status) {
		spec.expectStatus().isEqualTo(status)
			.expectBody()
			.jsonPath("$.status", status);

		return spec;
	}
}
