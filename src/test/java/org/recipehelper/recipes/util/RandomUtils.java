package org.recipehelper.recipes.util;

import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import org.recipehelper.recipes.domain.IngredientAmount;
import org.recipehelper.recipes.service.dto.CreateRecipeDto;
import org.recipehelper.recipes.service.dto.StepDto;

public class RandomUtils {

	public static CreateRecipeDto randRecipe(int steps, int ingredientsPerStep) {
		var stepList = (0 == steps) ? null : randSteps(steps, ingredientsPerStep);
		return new CreateRecipeDto(randStr(), randStr(), stepList);
	}

	public static List<StepDto> randSteps(int n, int ingredientsPerStep) {
		return IntStream
			.range(0, n)
			.mapToObj(i -> randStep(ingredientsPerStep))
			.toList();
	}

	public static StepDto randStep(int ingredients) {
		var ingredientsList = (0 == ingredients) ? null : randIngredients(ingredients);
		return new StepDto(null, randStr(), ingredientsList);
	}

	public static List<IngredientAmount> randIngredients(int n) {
		return IntStream
			.range(0, n)
			.mapToObj(i -> randIngredientAmount())
			.toList();
	}

	public static IngredientAmount randIngredientAmount() {
		return new IngredientAmount(randStr(), randDouble(), "unit");
	}

	public static double randDouble() {
		return Math.random() * 100;
	}

	public static String randStr() {
		return UUID.randomUUID().toString();
	}
}
