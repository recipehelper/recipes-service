package org.recipehelper.recipes.assertj;

import java.util.List;

import org.recipehelper.recipes.service.dto.FullRecipeDto;
import org.recipehelper.recipes.service.dto.RecipeSummaryDto;
import org.recipehelper.recipes.service.dto.StepDto;

public class RecipesAssertions {

	public static RecipeSummaryAssert assertThat(RecipeSummaryDto actual) {
		return new RecipeSummaryAssert(actual);
	}

	public static FullRecipeAssert assertThat(FullRecipeDto actual) {
		return new FullRecipeAssert(actual);
	}

	public static StepAssert assertThat(StepDto actual) {
		return new StepAssert(actual);
	}
}
