package org.recipehelper.recipes.assertj;

import java.util.List;

import org.assertj.core.api.ListAssert;
import org.recipehelper.recipes.domain.IngredientAmount;
import org.recipehelper.recipes.service.dto.StepDto;

public class IngredientAmountListAssert extends ListAssert<IngredientAmount> {

	public IngredientAmountListAssert(List<? extends IngredientAmount> actual) {
		super(actual);
	}

	public static IngredientAmountListAssert assertThat(List<? extends IngredientAmount> actual) {
		return new IngredientAmountListAssert(actual);
	}

	public IngredientAmountListAssert hasAllIngredientsFor(List<StepDto> steps) {
		isNotNull();

		if(null == steps) {
			isEmpty();
		} else {
			containsAll(getIngredients(steps));
		}

		return this;
	}

	private List<IngredientAmount> getIngredients(List<StepDto> steps) {
		return steps.stream()
			.map(StepDto::ingredients)
			.filter(ls -> null != ls)
			.flatMap(List::stream)
			.toList();
	}
}
