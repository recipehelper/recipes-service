package org.recipehelper.recipes.assertj;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.AbstractAssert;
import org.recipehelper.recipes.service.dto.CreateRecipeDto;
import org.recipehelper.recipes.service.dto.FullRecipeDto;

public class FullRecipeAssert extends AbstractAssert<FullRecipeAssert, FullRecipeDto> {

	protected FullRecipeAssert(FullRecipeDto actual) {
		super(actual, FullRecipeAssert.class);
	}

	public FullRecipeAssert hasId() {
		isNotNull();
		assertThat(actual.id()).isNotNull();

		return this;
	}

	public FullRecipeAssert hasDataFrom(CreateRecipeDto expected) {
		hasId();
		assertThat(actual.title()).isEqualTo(expected.title());
		assertThat(actual.authorId()).isEqualTo(expected.authorId());
		StepListAssert.assertThat(actual.steps()).hasDataFrom(expected.steps());
		IngredientAmountListAssert.assertThat(actual.ingredients()).hasAllIngredientsFor(expected.steps());

		return this;
	}
}
