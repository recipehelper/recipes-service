package org.recipehelper.recipes.assertj;

import java.util.List;
import java.util.stream.IntStream;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.ListAssert;
import org.recipehelper.recipes.service.dto.StepDto;

public class StepListAssert extends ListAssert<StepDto> {

	public StepListAssert(List<? extends StepDto> actual) {
		super(actual);
	}

	public static StepListAssert assertThat(List<? extends StepDto> actual) {
		return new StepListAssert(actual);
	}

	public StepListAssert hasDataFrom(List<StepDto> expected) {
		isNotNull();

		if(null == expected) {
			isEmpty();
		} else {
			hasSameSizeAs(expected);

			var stepAssert = Assertions.assertThat(actual, StepAssert.class);
			IntStream.range(0, expected.size())
				.forEach(i -> stepAssert.element(i).hasDataFrom(expected.get(i)));
		}

		return this;
	}
}
