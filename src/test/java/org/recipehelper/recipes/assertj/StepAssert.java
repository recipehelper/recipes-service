package org.recipehelper.recipes.assertj;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.AbstractAssert;
import org.recipehelper.recipes.service.dto.StepDto;

public class StepAssert extends AbstractAssert<StepAssert, StepDto> {

	public StepAssert(StepDto actual) {
		super(actual, StepAssert.class);
	}

	public StepAssert hasDataFrom(StepDto expected) {

		isInitialized();

		assertThat(actual.description()).isEqualTo(expected.description());
		if(null == expected.ingredients()) {
			assertThat(actual.ingredients()).isNotNull().isEmpty();
		} else {
			assertThat(actual.ingredients()).hasSameSizeAs(expected.ingredients());
			assertThat(actual.ingredients()).isEqualTo(expected.ingredients());
		}

		return this;
	}

	public StepAssert isInitialized() {

		isNotNull();

		assertThat(actual.id()).isNotNull();
		assertThat(List.of(
					actual.id(),
					actual.description(),
					actual.ingredients()))
			.withFailMessage("Step not initialized")
			.doesNotContainNull();

		return this;
	}
}
