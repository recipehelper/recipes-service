package org.recipehelper.recipes.assertj;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.AbstractAssert;
import org.recipehelper.recipes.service.dto.FullRecipeDto;
import org.recipehelper.recipes.service.dto.RecipeSummaryDto;

public class RecipeSummaryAssert extends AbstractAssert<RecipeSummaryAssert, RecipeSummaryDto> {

	protected RecipeSummaryAssert(RecipeSummaryDto actual) {
		super(actual, RecipeSummaryAssert.class);
	}

	public RecipeSummaryAssert hasDataFrom(FullRecipeDto expected) {
		isNotNull();

		assertThat(actual.id()).isEqualTo(expected.id());
		assertThat(actual.title()).isEqualTo(expected.title());
		assertThat(actual.ingredients()).containsAll(expected.ingredients());

		return this;
	}
}
