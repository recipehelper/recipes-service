package org.recipehelper.recipes.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.recipehelper.recipes.assertj.RecipesAssertions.assertThat;
import static org.recipehelper.recipes.util.RandomUtils.randRecipe;
import static org.recipehelper.recipes.util.WebTestClientUtils.expectError;
import static org.recipehelper.recipes.util.WebTestClientUtils.get;
import static org.recipehelper.recipes.util.WebTestClientUtils.getForCollection;
import static org.recipehelper.recipes.util.WebTestClientUtils.getForObject;
import static org.recipehelper.recipes.util.WebTestClientUtils.post;
import static org.recipehelper.recipes.util.WebTestClientUtils.postForObject;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.recipehelper.recipes.persistence.RecipeRepository;
import org.recipehelper.recipes.service.dto.CreateRecipeDto;
import org.recipehelper.recipes.service.dto.FullRecipeDto;
import org.recipehelper.recipes.service.dto.RecipeSummaryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@AutoConfigureWebTestClient
public class RecipeControllerTests {

	@Autowired private WebTestClient webClient;
	@Autowired private RecipeRepository recipeRepo;

	// HAS TO END WITH `/`
	public static final String BASE_URI = "/recipes/";

	@AfterEach
	void cleanup() {
		recipeRepo.deleteAll().block();
	}

	@Test
	void testCreateRecipeSimple() {
		testCreateRecipe(randRecipe(0, 0));
	}

	@Test
	void testCreateRecipeWithSteps() {
		testCreateRecipe(randRecipe(5, 0));
	}

	@Test
	void testCreateRecipeWithStepsWithIngredients() {
		testCreateRecipe(randRecipe(5, 3));
	}

	@Test
	void testCreateRecipeInvalidContentType() {
		webClient.post()
			.uri(uri())
			.contentType(MediaType.TEXT_PLAIN)
			.bodyValue("INVALID")
			.exchange()
			.expectStatus().isEqualTo(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
			.expectBody()
			.jsonPath("$.status").isEqualTo(415);
	}

	void testCreateRecipe(CreateRecipeDto createRecipeDto) {
		post(webClient, uri(), createRecipeDto)
			.expectStatus().isCreated()
			.expectBody(FullRecipeDto.class)
			.consumeWith(response ->
					assertThat(response.getResponseBody()).hasDataFrom(createRecipeDto));
	}

	@Test
	void testGetAllRecipes() {
		assertThat(getForCollection(webClient, uri(), RecipeSummaryDto.class))
			.isEmpty();

		var created = IntStream.range(0, 5)
			.mapToObj(i -> randRecipe(0, 0))
			.map(r -> postForObject(webClient, uri(), r, FullRecipeDto.class))
			.toList();

		var found = getForCollection(webClient, uri(), RecipeSummaryDto.class);

		assertThat(found).hasSameSizeAs(created);

		created.forEach(crtd -> {
			findById(found, crtd.id()).ifPresentOrElse(
					f -> assertThat(f).hasDataFrom(crtd),
					() -> fail());
		});
	}

	@Test
	void testGetRecipe() {

		var resp = get(webClient, uri("INVALID_ID"));
		expectError(resp, HttpStatus.NOT_FOUND.value());

		var first = postForObject(webClient, uri(), randRecipe(0, 0), FullRecipeDto.class);
		testGet(first);

		var createdList = IntStream.range(0, 10)
			.mapToObj(i -> postForObject(webClient, uri(), randRecipe(0, 0), FullRecipeDto.class))
			.toList();

		testGet(first);
		testGet(createdList.get(createdList.size() - 1));
		testGet(createdList.get(createdList.size() / 2));
	}

	void testGet(FullRecipeDto source) {
		assertThat(getForObject(
					webClient,
					uri(source.id()),
					FullRecipeDto.class))
			.isEqualTo(source);
	}

	@Test
	void testDeleteRecipe() {
		var req = webClient.delete()
			.uri(uri("FAKE_ID"))
			.exchange();
		expectError(req, HttpStatus.NOT_FOUND.value());

		var created = postForObject(webClient, uri(), randRecipe(0, 0), FullRecipeDto.class);

		webClient.delete()
			.uri(uri(created.id()))
			.exchange()
			.expectStatus().isNoContent();

		req = get(webClient, uri(created.id()));
		expectError(req, HttpStatus.NOT_FOUND.value());
	}

	private Optional<RecipeSummaryDto> findById(List<RecipeSummaryDto> ls, String id) {
		return ls.stream()
			.filter(r -> r.id().equals(id))
			.findAny();
	}

	public String uri(String...slugs) {
		return BASE_URI + String.join("/", slugs);
	}
}
