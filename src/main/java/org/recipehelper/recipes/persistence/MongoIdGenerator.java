package org.recipehelper.recipes.persistence;

import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

@Component
public class MongoIdGenerator implements IdGenerator<String> {

	@Override
	public String next() {
		return ObjectId.get().toString();
	}
}
