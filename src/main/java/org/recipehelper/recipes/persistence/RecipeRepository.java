package org.recipehelper.recipes.persistence;

import org.recipehelper.recipes.domain.Recipe;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends ReactiveCrudRepository<Recipe, String> {}
