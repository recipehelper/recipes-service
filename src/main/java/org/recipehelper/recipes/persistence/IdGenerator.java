package org.recipehelper.recipes.persistence;

public interface IdGenerator<T> {
	T next();
}
