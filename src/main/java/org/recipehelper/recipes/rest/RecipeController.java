package org.recipehelper.recipes.rest;

import java.net.URI;

import org.recipehelper.recipes.service.RecipeService;
import org.recipehelper.recipes.service.dto.CreateRecipeDto;
import org.recipehelper.recipes.service.dto.FullRecipeDto;
import org.recipehelper.recipes.service.dto.RecipeSummaryDto;
import org.recipehelper.recipes.service.dto.StepDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/recipes")
@RequiredArgsConstructor
public class RecipeController {

	private final RecipeService recipeService;

	@PostMapping
	public Mono<ResponseEntity<FullRecipeDto>> createRecipe(
			@RequestBody Mono<CreateRecipeDto> recipe,
			ServerHttpRequest request) {

		return recipeService
			.createRecipe(recipe)
			.map(r -> ResponseEntity.created(createdURI(request, r)).body(r));
	}

	@GetMapping
	public Flux<RecipeSummaryDto> listRecipes() {
		return recipeService.listRecipes();
	}

	@GetMapping("/{recipeId}")
	public Mono<FullRecipeDto> getRecipe(@PathVariable String recipeId) {
		return recipeService.getRecipe(recipeId);
	}

	@DeleteMapping("/{recipeId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Void> deleteRecipe(@PathVariable String recipeId) {
		return recipeService.deleteRecipe(recipeId);
	}

	@PostMapping("/{recipeId}/steps")
	public Mono<ResponseEntity<StepDto>> addStep(
			@PathVariable String recipeId,
			@RequestBody Mono<StepDto> dto,
			ServerHttpRequest request) {

		return recipeService.addStep(recipeId, dto)
			.map(s -> ResponseEntity.created(createdURI(request, recipeId, s)).body(s));
	}

	@DeleteMapping("/{recipeId}/steps/{stepId}")
	public Mono<ResponseEntity<Object>> deleteStep(@PathVariable String recipeId, @PathVariable String stepId) {
		return recipeService.deleteStep(recipeId, stepId)
			.then(Mono.fromSupplier(() -> ResponseEntity.noContent().build()));
	}

	private URI createdURI(ServerHttpRequest request, FullRecipeDto created) {
		return URI.create(String.format(
					"%s/%s",
					request.getURI(),
					created.id()));
	}

	private URI createdURI(ServerHttpRequest request, String recipeId, StepDto step) {
		return URI.create(String.format(
					"%s/%s/steps/%s",
					request.getURI(),
					recipeId,
					step.id()));
	}
}
