package org.recipehelper.recipes.rest;

import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {

	private Map<Class<?>, Object> responses = Map.of(
			NoSuchElementException.class, HttpStatus.NOT_FOUND.value());

	@Override
	public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions opts) {
		Throwable error = super.getError(request);

		Map<String, Object> attrs = super.getErrorAttributes(request, opts);

		attrs.compute("status", (k, v) -> responses.getOrDefault(error.getClass(), v));
		attrs.put("message", error.getMessage());

		return attrs;
	}
}
