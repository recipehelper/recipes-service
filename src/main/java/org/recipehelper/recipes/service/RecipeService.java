package org.recipehelper.recipes.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

import org.recipehelper.recipes.domain.Recipe;
import org.recipehelper.recipes.domain.RecipeStep;
import org.recipehelper.recipes.persistence.IdGenerator;
import org.recipehelper.recipes.persistence.RecipeRepository;
import org.recipehelper.recipes.service.dto.CreateRecipeDto;
import org.recipehelper.recipes.service.dto.FullRecipeDto;
import org.recipehelper.recipes.service.dto.RecipeSummaryDto;
import org.recipehelper.recipes.service.dto.StepDto;
import org.recipehelper.recipes.service.dto.UpdateRecipeDto;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class RecipeService {

	private final RecipeRepository recipeRepo;
	private final IdGenerator<String> idGenerator;

	public Mono<FullRecipeDto> createRecipe(Mono<CreateRecipeDto> dto) {
		return dto
			.map(crd -> {
				var recipe = new Recipe(crd.title(), crd.authorId());
				if(null != crd.steps()) {
					crd.steps().stream()
						.map(this::createStep)
						.forEach(recipe.getSteps()::add);
				}

				return recipe;
			})
			.flatMap(recipeRepo::save)
			.map(this::toDto);
	}

	public Mono<FullRecipeDto> getRecipe(String id) {
		return recipeRepo.findById(id)
			.map(this::toDto)
			.switchIfEmpty(Mono.error(() -> new NoSuchElementException()));
	}

	public Flux<RecipeSummaryDto> listRecipes() {
		return recipeRepo.findAll()
			.map(r -> new RecipeSummaryDto(r.getId(), r.getTitle(), r.getIngredients()));
	}

	public Mono<Void> deleteRecipe(String recipeId) {
		return Mono.just(recipeId)
			.filterWhen(recipeRepo::existsById)
			.switchIfEmpty(Mono.error(() -> new NoSuchElementException()))
			.flatMap(recipeRepo::deleteById);
	}

	public Mono<FullRecipeDto> updateRecipe(String recipeId, Mono<UpdateRecipeDto> dto) {
		return recipeRepo.findById(recipeId)
			.zipWith(dto, (recipe, d) -> {
				recipe.setTitle(d.title());
				return recipe;
			})
			.flatMap(recipeRepo::save)
			.map(this::toDto)
			.switchIfEmpty(Mono.error(() -> new NoSuchElementException()));
	}

	public Mono<StepDto> addStep(String recipeId, Mono<StepDto> dto) {
		return dto
			.map(this::createStep)
			.flatMap(step -> recipeRepo
					.findById(recipeId)
					.doOnNext(r -> r.addStep(step))
					.flatMap(recipeRepo::save)
					.map(r -> step))
			.map(this::toDto)
			.switchIfEmpty(Mono.error(() -> new NoSuchElementException()));
	}

	public Mono<Void> deleteStep(String recipeId, String stepId) {
		return recipeRepo.findById(recipeId)
			.filter(r -> r.deleteStep(stepId))
			.flatMap(recipeRepo::save)
			.switchIfEmpty(Mono.error(() -> new NoSuchElementException()))
			.then();
	}

	private RecipeStep createStep(StepDto dto) {
		return new RecipeStep(
				idGenerator.next(),
				dto.description(),
				nonNull(dto.ingredients(), () -> List.of()));
	}

	private List<StepDto> toDtos(List<RecipeStep> steps) {
		return steps.stream()
			.map(this::toDto)
			.toList();
	}

	private FullRecipeDto toDto(Recipe r) {
		return new FullRecipeDto(
						r.getId(),
						r.getTitle(),
						r.getAuthorId(),
						r.getIngredients(),
						toDtos(r.getSteps()));
	}

	private StepDto toDto(RecipeStep step) {
		return new StepDto(
				step.getId(),
				step.getDescription(),
				step.getIngredients());
	}

	private <T> T nonNull(T val, Supplier<T> dfault) {
		return null == val ? dfault.get() : val;
	}
}
