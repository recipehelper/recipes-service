package org.recipehelper.recipes.service.dto;

import java.util.List;

import org.recipehelper.recipes.domain.IngredientAmount;

public record StepDto(String id, String description, List<IngredientAmount> ingredients) {}
