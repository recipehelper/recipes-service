package org.recipehelper.recipes.service.dto;

import java.util.List;

import org.recipehelper.recipes.domain.IngredientAmount;

public record RecipeSummaryDto(String id, String title, List<IngredientAmount> ingredients) {}
