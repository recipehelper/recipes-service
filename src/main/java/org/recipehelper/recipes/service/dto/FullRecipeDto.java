package org.recipehelper.recipes.service.dto;

import java.util.List;

import org.recipehelper.recipes.domain.IngredientAmount;

public record FullRecipeDto(String id, String title, String authorId, List<IngredientAmount> ingredients, List<StepDto> steps) {}
