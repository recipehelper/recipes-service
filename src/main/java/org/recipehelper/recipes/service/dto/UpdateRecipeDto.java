package org.recipehelper.recipes.service.dto;

public record UpdateRecipeDto(String title) {}
