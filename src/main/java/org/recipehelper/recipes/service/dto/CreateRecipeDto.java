package org.recipehelper.recipes.service.dto;

import java.util.List;

public record CreateRecipeDto(String title, String authorId, List<StepDto> steps) {}
