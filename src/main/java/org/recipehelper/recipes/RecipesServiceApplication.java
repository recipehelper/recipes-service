package org.recipehelper.recipes;

import org.recipehelper.recipes.persistence.RecipeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RecipesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecipesServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner clearDb(RecipeRepository repo) {
		return args -> {
			repo.deleteAll().block();
		};
	}
}
