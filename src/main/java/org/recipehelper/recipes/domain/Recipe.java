package org.recipehelper.recipes.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Document
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Recipe {

	@Id private String id;

	@NonNull private String title;

	@NonNull private String authorId;

	@NonNull private List<RecipeStep> steps;

	public Recipe(String title, String authorId) {
		this.title = title;
		this.authorId = authorId;
		this.steps = new ArrayList<>();
	}

	public List<IngredientAmount> getIngredients() {
		return null == steps ? List.of() : steps.stream()
			.map(RecipeStep::getIngredients)
			.collect(ArrayList::new, List::addAll, List::addAll);
	}

	public RecipeStep addStep(RecipeStep step) {
		this.steps.add(step);
		return step;
	}

	public boolean deleteStep(String id) {
		return steps.removeIf(s -> s.getId().equals(id));
	}
}
