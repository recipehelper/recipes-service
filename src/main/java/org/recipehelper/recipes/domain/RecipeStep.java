package org.recipehelper.recipes.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecipeStep {

	private String id;

	private String description;

	private List<IngredientAmount> ingredients;
}
