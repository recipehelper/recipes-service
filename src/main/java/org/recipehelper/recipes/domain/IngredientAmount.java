package org.recipehelper.recipes.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IngredientAmount {

	private String ingredient;

	private double amount;

	private String unit;
}
